﻿using System;

namespace simpledecisions_example_task_20
{
    class Program
    {
        static void Main(string[] args)
        {  
            //PART A START
            var num = (Double) 0;
            var num2 = (Double) 0;
            Console.WriteLine("\n=============================");
            Console.WriteLine("Hello, please enter a number!\n");

            num = Double.Parse(Console.ReadLine());

            Console.WriteLine($"\nYou entered {num}, please enter another number!\n");

            num2 = Double.Parse(Console.ReadLine());

            {
                if (num > num2)
                {
                    Console.WriteLine($"\nNumber 1 ({num}) is LARGER than Number 2 ({num2}).\n");
                }
                
                else if (num < num2)
                {
                    Console.WriteLine($"\nNumber 1 ({num}) is SMALLER than Number 2 ({num2}).\n");
                }

                else if (num == num2)
                {
                    Console.WriteLine($"\nNumber 1 ({num}) is EQUAL TO Number 2 ({num2}).\n");
                }

                else
                {
                    Console.WriteLine("\nMaths Error.\n\n");
                }
            }
            //PART A END

            //PART B START
            var pswrd = (String) null;
            var cnfrmPswrd = (String) null;

            Retry:

            Console.WriteLine("Please enter your new password! It must be 8 characters long.\n");

            pswrd = Console.ReadLine();

            {
                if (pswrd.Length > 7)
                    {
                    Console.WriteLine("\nPlease reconfirm your password!\n");

                    cnfrmPswrd = Console.ReadLine();

                        {
                        if (pswrd == cnfrmPswrd)
                            {
                            Console.WriteLine("\nThank you, your new password has been set.\n");
                            }
                    
                        else if (pswrd != cnfrmPswrd)
                            {
                            Console.WriteLine("\nThe passwords do not match, try again.\n");
                            goto Retry;
                            }
                        }
                    }
                
                else
                    {
                    Console.WriteLine("\nYour new password is too short, try again.\n");
                    goto Retry;
                    }
            }
            //PART B END
            
            //PART C START
            var newPswrd = (String) null;

            Retry2:
            
            Console.WriteLine("\nEnter a newer password to reset your old password.\n");
            
            newPswrd = Console.ReadLine();
            {
                if (newPswrd.Length < 8)
                {
                    Console.WriteLine("\nYour new password is too short, try again.\n");
                    goto Retry2;
                }
            }
                
            Console.WriteLine("\nPlease reconfirm your password!\n");

            cnfrmPswrd = Console.ReadLine();
            {
                if (newPswrd != cnfrmPswrd)
                {
                    Console.WriteLine("\nThe passwords do not match, try again.\n");
                    goto Retry2;
                }  
            }
            
            pswrd = newPswrd.Replace("{newPswrd}", ""); 
            newPswrd = null;

            Console.WriteLine($"\nThe password is now: {pswrd}.\n");
                    
        }
    }
}
